CASCADE-RANGE <StdCommRange(id=None, asn=2056, from=1, to=666, descr=None)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=0, pos=0, descr=Prepends n times to AOL)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=n)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=1, pos=0, descr=Prepends n times to AOL)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=n)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=665, pos=0, descr=Do not announce to AOL)>
                 <StdComm.Announcement(id=None, std_comm=None, scope=None, skip=True, blackhole=False)>

NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>
NORMAL: <StdCommRange(id=None, asn=4589, from=4, to=None, descr=Prefix restricted to one subconfederatioin)>

CASCADE-RANGE <StdCommRange(id=None, asn=1273, from=39700, to=39709, descr=None)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=0, pos=0, descr=Do not announce to NYIIX)>
                 <StdComm.Announcement(id=None, std_comm=None, scope=None, skip=True, blackhole=False)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=1, pos=0, descr=Prepends n times to NYIIX)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=n)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=2, pos=0, descr=Prepends n times to NYIIX)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=n)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=3, pos=0, descr=Prepends n times to NYIIX)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=n)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=9, pos=0, descr=Announce to NYIIX)>
                 <StdComm.Announcement(id=None, std_comm=None, scope=None, skip=False, blackhole=False)>

------------------------------------------------
EGAL|
----
CASCADE-RANGE <StdCommRange(id=None, asn=4589, from=1410, to=3410, descr=None)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=0, pos=0, descr=Prepends n times to a high capacity IXP or private peer)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=1)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=1000, pos=0, descr=Prepends n times to a high capacity IXP or private peer)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=2)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=2000, pos=0, descr=Prepends n times to a high capacity IXP or private peer)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=3)>
CASCADE-RANGE <StdCommRange(id=None, asn=4589, from=8410, to=9410, descr=None)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=0, pos=0, descr=Do not prepend to high capacity IXP or private peer)>
                 <StdComm.Prepend(id=None, std_comm=None, scope=None, times=8)>
        CASCASE-FIELD <StdCommField(id=None, std=None, offset=1000, pos=0, descr=Do not announce to high capacity IXP or private peer)>
                 <StdComm.Announcement(id=None, std_comm=None, scope=None, skip=True, blackhole=False)>