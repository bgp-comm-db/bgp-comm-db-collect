'''
Created on 15.02.2015

@author: raabf
'''

if __name__ == "__main__" and __package__ is None:
    __package__ = "bgp_comm_db_collect"  # PEP 366

import sys
sys.path.append('/media/osshare-crypt/git/bgp-comm-db-framework')

from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.tools import *
from bgp_comm_db_collect.insert import *
from datetime import datetime as DateTime
from marshmallow import pprint
from sqlalchemy import desc
from sqlalchemy.orm import aliased
from sqlalchemy_utils.functions import *
from tabulate import tabulate
import click
import dateutil.parser
import inspect
import os
import time

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"

@click.group()
def main():
    pass

insert_opt_rollback_trans = True

@main.group()
@click.option('--rollback-trans/--no-rollback-trans', default=True,
              help="Use a special transaction "
              "which will be rolled back at the end. "
              "In the DB nothing will be changed.")
def insert(rollback_trans):
    global insert_opt_rollback_trans
    insert_opt_rollback_trans = rollback_trans


@insert.command()
@click.argument('path', type=click.Path(exists=True))
def whois(path):
    global insert_opt_rollback_trans
    with WhoisInsert(path, insert_opt_rollback_trans) as whois_insert:
        whois_insert.update()


@insert.command(name='dir')
@click.argument('path', type=click.Path(exists=True))
def dir_(path):
    global insert_opt_rollback_trans
    with DirInsert(path, insert_opt_rollback_trans) as dir_insert:
        dir_insert.as_dir()

@insert.command('csv')
def csv_insert():
    CsvInsert.inserts()


@insert.command()
def xml():
    with TaxonomyXML(config.get('PATHS', 'data_dir') +
                     "/CommunityTaxonomy.xml") as taxonomy:
        taxonomy.xml_insert()

if __name__ == '__main__':
    main()
