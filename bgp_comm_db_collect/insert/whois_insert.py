"""
"""

from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.connection_session import *
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.keyed_set import *
from bgp_comm_db.progressbar import Bar
from bgp_comm_db.progressbar import FormatLabel
from bgp_comm_db.progressbar import Percentage
from bgp_comm_db.progressbar import ProgressBar
from bgp_comm_db.tools.regex_detection import *
from datetime import datetime as DateTime
from logging import critical
from logging import error
from logging import exception
from logging import info
from logging import log
from logging import warning
from sqlalchemy.exc import IntegrityError
from tabulate import tabulate
import codecs
import copy
import os
import re

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['WhoisInsert']


class WhoisInsert(ConnectionSession):

    def __init__(self, autnum_file: str, rollback_trans: bool=True):
        super(WhoisInsert, self).__init__(rollback_trans)
        self.session = None
        self.connection = None
        self.trans = None
        ctime = os.path.getctime(autnum_file)
        self.datetime = (DateTime.utcfromtimestamp(ctime)
                         ).replace(microsecond=0)

        self.autnum_file = autnum_file

    def as_generator(self):
        as_regex = re.compile('^aut-num:\s+AS(\d+).*$', re.MULTILINE)
        simple_comm_regex = re.compile(
            '^remarks:[\s\|]+(\d+):(\d+)[\s\-=:,]+(.*)$',
            re.MULTILINE)
        simple_comm_regex_end_letter = re.compile(
            '^remarks:[\s\|]+(\d+):(\d+)[A-Za-z][\s\-=:,]+(.*)$',
            re.MULTILINE)

        with codecs.open(self.autnum_file, 'r', encoding='ascii',
                         errors='replace') as autnum_file:
            asn_def = None
            ases_def = None
            yield_values = []

            for i, lineln in enumerate(autnum_file):
                line = lineln.rstrip("\n")

                # Match AS

                as_match = as_regex.match(line)

                if as_match:
                    if yield_values:
                        yield (ases_def, yield_values)
                    yield_values = []

                    asn_def = int(as_match.group(1))
                    ases_def = Ases.get_at_datetime(
                        self.session,
                        asn_def,
                        self.datetime)
                    continue

                # Match simple comm

                simple_comm_match = simple_comm_regex.match(line)

                if simple_comm_match:
                    asn = int(simple_comm_match.group(1))
                    val = int(simple_comm_match.group(2))
                    description = simple_comm_match.group(3)
                    from_value = StdValue(asn, val, self.datetime)

                    from_value.description = description
                    from_value.to_value = None

                    yield_values.append(from_value)

                # Match Comm with an letter at the end

                simple_comm_end_letter_match = (simple_comm_regex_end_letter.
                                                match(line))

                if simple_comm_end_letter_match:
                    asn = int(simple_comm_end_letter_match.group(1))
                    val = int(simple_comm_end_letter_match.group(2))*10
                    description = simple_comm_end_letter_match.group(3)
                    from_value = StdValue(asn, val, self.datetime)
                    to_value = StdValue(asn, min(val+9, (2**16)-1),
                                        self.datetime)

                    from_value.description = description
                    from_value.to_value = to_value

                    yield_values.append(from_value)

        if yield_values:
            yield (ases_def, yield_values)

    @staticmethod
    def _values_and_description_eq(obj) -> str:
        if isinstance(obj, StdValue):
            return (obj.str_value +
                    obj.description)
        elif isinstance(obj, StdCommRange):
            return (obj.from_value.str_value +
                    obj.description)
        else:
            raise Exception("Unexpected Type.")

    @staticmethod
    def _values_eq(obj) -> str:
        if isinstance(obj, StdValue):
            return (obj.str_value)
        elif isinstance(obj, StdCommRange):
            return (obj.from_value.str_value)
        else:
            raise Exception("Unexpected Type.")

    @typechecked
    def insert_row(self, ases: Ases, std_value: StdValue, deleted: bool):

        try:
            with self.session.begin_nested():

                std_range = StdCommRange(
                    from_value=std_value,
                    datetime=self.datetime,
                    ases=ases,
                    to_value=std_value.to_value,
                    description=std_value.description,
                    reference="ripe whois",
                    observed_only=False,
                    source=StdSource.whois,
                    deleted=deleted)
                print(std_range)
                self.session.add(std_range)
            self.session.commit()

        except IntegrityError as ie:
            warning("INTEGRITY-ERROR:", ases)
            warning(ie)
            return

        print(std_range)

        if std_value.to_value is None:
            det = RegexDetection(
                description=std_value.description,
                value=std_value,
                session=self.session)

            det.taxonomy(std_comm=std_range)

            print(
                tabulate(
                    std_range.taxonomies_scopes_to_dict(),
                    headers="keys"))

        else:
            det = RegexDetection(
                description=std_value.description,
                value=std_value,
                session=self.session)

            scopes = det.scope()

            if (std_range.to_val is not None and
                    std_range.from_val + 0 <= std_range.to_val):
                field0 = StdCommField(0, 0, self.datetime, range_=std_range,
                                      description="GUESSED:"
                                      " Do not prepend.",
                                      deleted=deleted)
                self.session.add(field0)
            if (std_range.to_val is not None and
                    std_range.from_val + 1 <= std_range.to_val):
                field1 = StdCommField(1, 0, self.datetime, range_=std_range,
                                      description="GUESSED:"
                                      " Prepend once.",
                                      deleted=deleted)
                self.session.add(field1)
            if (std_range.to_val is not None and
                    std_range.from_val + 2 <= std_range.to_val):
                field2 = StdCommField(2, 0, self.datetime, range_=std_range,
                                      description="GUESSED:"
                                      " Prepend twice.",
                                      deleted=deleted)
                self.session.add(field2)
            if (std_range.to_val is not None and
                    std_range.from_val + 3 <= std_range.to_val):
                field3 = StdCommField(3, 0, self.datetime, range_=std_range,
                                      description="GUESSED:"
                                      " Prepend three times.",
                                      deleted=deleted)
                self.session.add(field3)
            if (std_range.to_val is not None and
                    std_range.from_val + 0 <= std_range.to_val):
                field9 = StdCommField(9, 0, self.datetime, range_=std_range,
                                      description="GUESSED:"
                                      " Do not announce.",
                                      deleted=deleted)
                self.session.add(field9)

            if not scopes:
                scopes = [None]

            for scope in scopes:
                if (std_range.to_val is not None and
                        std_range.from_val + 0 <= std_range.to_val):
                    prep0 = StdComm.Prepend(
                        times=0,
                        std_comm=field0,
                        scope=scope,
                        auto_detected=True,
                        auto_detected_scope=True)
                    self.session.add(prep0)
                if (std_range.to_val is not None and
                        std_range.from_val + 1 <= std_range.to_val):
                    prep1 = StdComm.Prepend(
                        times=1,
                        std_comm=field1,
                        scope=scope,
                        auto_detected=True,
                        auto_detected_scope=True)
                    self.session.add(prep1)
                if (std_range.to_val is not None and
                        std_range.from_val + 2 <= std_range.to_val):
                    prep2 = StdComm.Prepend(
                        times=0,
                        std_comm=field2,
                        scope=scope,
                        auto_detected=True,
                        auto_detected_scope=True)
                    self.session.add(prep2)
                if (std_range.to_val is not None and
                        std_range.from_val + 3 <= std_range.to_val):
                    prep3 = StdComm.Prepend(
                        times=0,
                        std_comm=field3,
                        scope=scope,
                        auto_detected=True,
                        auto_detected_scope=True)
                    self.session.add(prep3)
                if (std_range.to_val is not None and
                        std_range.from_val + 9 <= std_range.to_val):
                    anno9 = StdComm.Announcement(
                        skip=True,
                        blackhole=False,
                        std_comm=field9,
                        scope=scope,
                        auto_detected=True,
                        auto_detected_scope=True)
                    self.session.add(anno9)

                self.session.commit()

                print(
                    tabulate(
                            (field.to_dict() for field in std_range.fields),
                        headers="keys"))

    def update(self):
        for ases, values in self.as_generator():
            fks_values = FrozenKeyedSet(
                values, key=WhoisInsert._values_and_description_eq)

            ranges = StdComm.get_all_ranges(self.session, ases,
                                            source=StdSource.whois)
            fks_ranges = FrozenKeyedSet(
                ranges, key=WhoisInsert._values_and_description_eq)

            to_update = fks_values - fks_ranges
            changed = fks_ranges - fks_values

            to_update_value_eq = FrozenKeyedSet(
                to_update, key=WhoisInsert._values_eq)
            changed_value_eq = FrozenKeyedSet(
                changed, key=WhoisInsert._values_eq)

            to_delete = changed_value_eq - to_update_value_eq
            # changed - to_updated

            print(tabulate((obj.to_dict() for obj in (to_update)),
                           headers='keys'))
            print(tabulate((obj.to_dict() for obj in (changed)),
                           headers='keys'))
            print(tabulate((obj.to_dict() for obj in (to_delete)),
                           headers='keys'))

            for std_value in to_update:
                self.insert_row(ases, std_value, False)

            for std_range in to_delete:
                std_value = copy.deepcopy(std_range.from_value)
                std_value.to_value = copy.deepcopy(std_range.to_value)
                std_value.description = std_range.description

                self.insert_row(ases, std_value, True)

    def insert(self):

        as_regex = re.compile('^aut-num:\s+AS(\d+).*$', re.MULTILINE)
        simple_comm_regex = re.compile(
            '^remarks:[\s\|]+(\d+):(\d+)[\s\-=:,]+(.*)$',
            re.MULTILINE)
        simple_comm_regex_end_letter = re.compile(
            '^remarks:[\s\|]+(\d+):(\d+)[A-Za-z][\s\-=:,]+(.*)$',
            re.MULTILINE)

        with codecs.open(self.autnum_file, 'r', encoding='ascii',
                         errors='replace') as autnum_file:
            asn_def = None
            ases_def = None

            for i, lineln in enumerate(autnum_file):
                line = lineln.rstrip("\n")

                # Match AS

                as_match = as_regex.match(line)

                if as_match:
                    asn_def = int(as_match.group(1))
                    ases_def = Ases.get_at_datetime(
                        self.session,
                        asn_def,
                        self.datetime)
                    continue

                # Match simple comm

                simple_comm_match = simple_comm_regex.match(line)

                if simple_comm_match:
                    asn = int(simple_comm_match.group(1))
                    val = int(simple_comm_match.group(2))
                    description = simple_comm_match.group(3)
                    from_value = StdValue(asn, val, self.datetime)

                    try:
                        with self.session.begin_nested():
                            std_range = StdCommRange(
                                from_value=from_value,
                                datetime=self.datetime,
                                ases=ases_def,
                                to_value=None,
                                description=description,
                                reference="ripe whois",
                                observed_only=False,
                                source=StdSource.whois)
                            self.session.add(std_range)
                        self.session.commit()
                    except IntegrityError as ie:
                        print("INTEGRITY-ERROR:", asn_def)
                        print(ie)
                        continue

                    det = RegexDetection(
                        description=description,
                        value=from_value,
                        session=self.session)

                    det.taxonomy(std_comm=std_range)

                    print(std_range)
                    print(
                        tabulate(
                            std_range.taxonomies_scopes_to_dict(),
                            headers="keys"))

                # Match Comm with an letter at the end

                simple_comm_end_letter_match = (simple_comm_regex_end_letter.
                                                match(line))

                if simple_comm_end_letter_match:
                    asn = int(simple_comm_end_letter_match.group(1))
                    val = int(simple_comm_end_letter_match.group(2))*10
                    description = simple_comm_end_letter_match.group(3)
                    from_value = StdValue(asn, val, self.datetime)
                    to_value = StdValue(asn, val+9, self.datetime)

                    try:
                        with self.session.begin_nested():
                            std_range = StdCommRange(
                                from_value=from_value,
                                to_value=to_value,
                                datetime=self.datetime,
                                ases=ases_def,
                                description=description,
                                reference="ripe whois",
                                observed_only=False,
                                source=StdSource.whois)
                            self.session.add(std_range)
                        self.session.commit()
                    except IntegrityError as ie:
                        print("INTEGRITY-ERROR:", asn_def)
                        print(ie)
                        continue

                    det = RegexDetection(
                        description=description,
                        value=from_value,
                        session=self.session)

                    scopes = det.scope()

                    field0 = StdCommField(0, 0, self.datetime, range_=std_range,
                                          description="GUESSED:"
                                          " Do not prepend.")
                    self.session.add(field0)
                    field1 = StdCommField(1, 0, self.datetime, range_=std_range,
                                          description="GUESSED:"
                                          " Prepend once.")
                    self.session.add(field1)
                    field2 = StdCommField(2, 0, self.datetime, range_=std_range,
                                          description="GUESSED:"
                                          " Prepend twice.")
                    self.session.add(field2)
                    field3 = StdCommField(3, 0, self.datetime, range_=std_range,
                                          description="GUESSED:"
                                          " Prepend three times.")
                    self.session.add(field3)
                    field9 = StdCommField(9, 0, self.datetime, range_=std_range,
                                          description="GUESSED:"
                                          " Do not announce.")
                    self.session.add(field9)

                    if not scopes:
                        scopes = [None]

                    for scope in scopes:
                        prep0 = StdComm.Prepend(
                            times=0,
                            std_comm=field0,
                            scope=scope,
                            auto_detected=True,
                            auto_detected_scope=True)
                        self.session.add(prep0)
                        prep1 = StdComm.Prepend(
                            times=1,
                            std_comm=field1,
                            scope=scope,
                            auto_detected=True,
                            auto_detected_scope=True)
                        self.session.add(prep1)
                        prep2 = StdComm.Prepend(
                            times=0,
                            std_comm=field2,
                            scope=scope,
                            auto_detected=True,
                            auto_detected_scope=True)
                        self.session.add(prep2)
                        prep3 = StdComm.Prepend(
                            times=0,
                            std_comm=field3,
                            scope=scope,
                            auto_detected=True,
                            auto_detected_scope=True)
                        self.session.add(prep3)
                        anno9 = StdComm.Announcement(
                            skip=True,
                            blackhole=False,
                            std_comm=field9,
                            scope=scope,
                            auto_detected=True,
                            auto_detected_scope=True)
                        self.session.add(anno9)

                        self.session.commit()

                    print(std_range)
                    print(
                        tabulate(
                            (field.to_dict() for field in std_range.fields),
                            headers="keys"))
