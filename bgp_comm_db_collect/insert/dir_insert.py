"""
"""

from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.connection_session import *
from bgp_comm_db.tools.regex_detection import RegexDetection
from bgp_comm_db.progressbar import Bar
from bgp_comm_db.progressbar import FormatLabel
from bgp_comm_db.progressbar import Percentage
from bgp_comm_db.progressbar import ProgressBar
from tabulate import tabulate
import csv
import dateutil.parser
import os
import re

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['DirInsert']

class DirInsert(ConnectionSession):

    """docstring for DirInsert"""

    def __init__(self, path, rollback_trans: bool=True):
        super(DirInsert, self).__init__(rollback_trans)
        self.path = os.path.realpath(path)
        self.def_as = None
        self.datetime = None
        self.reference = None

        self.connection = None
        self.trans = None
        self.session = None

    def as_dir(self):
        s_as_dir = os.path.basename(self.path)
        l_as_dir = s_as_dir.split('--')
        s_as = l_as_dir[0]
        s_datetime = l_as_dir[1]

        self.datetime = dateutil.parser.parse(s_datetime)
        det = RegexDetection(s_as, StdValue(0, 0, self.datetime), self.session)

        ases = det.scope_as()

        if not ases:
            raise ValueError("The PATH does not contain the ASN.")

        self.def_as = ases[0]

        with open(os.path.join(self.path, 'reference.txt'), mode='r',
                  closefd=True) as reference_file:
            self.reference = reference_file.readline()

        for filedir in os.listdir(self.path):
            if os.path.isdir(os.path.join(self.path, filedir)):
                self.field_folder(os.path.join(self.path, filedir))

            elif (os.path.isfile(os.path.join(self.path, filedir)) and
                  filedir.endswith('.csv')):
                self.csv_file(os.path.join(self.path, filedir))

    def _skip_row(self, row) -> bool:
        if not any(row):
            return True
        # regex = re.compile(
        #         '^[^;]+;[^;]*$', re.MULTILINE | re.IGNORECASE)
        # m_regex = regex.match(row)
        # if not m_regex:
        #     return True
        if row['description'] is None or ';' in row['description']:
            return True
        return False

    def csv_file(self, csv_file):

        with open(csv_file, mode='r', closefd=True) as file:

            reader = csv.DictReader(file, skipinitialspace=True,
                                    fieldnames=('community', 'description'),
                                    delimiter=';')

            i = 0
            for i, row in enumerate(reader):

                if self._skip_row(row):
                    continue

                value = StdValue(row['community'], self.datetime)

                range_ = StdCommRange(ases=self.def_as,
                                      from_value=value,
                                      observed_only=False,
                                      datetime=self.datetime,
                                      reference=self.reference,
                                      description=row['description'],
                                      source=StdSource.manual)

                self.session.add(range_)
                self.session.commit()

                det = RegexDetection(row['description'], value, self.session)
                taxos = det.taxonomy(range_)

                print(range_)
                print(tabulate(range_.taxonomies_scopes_to_dict(), headers="keys"))

    def _find_text_variables(self, string: str) -> (str, str, str):

        positions = dict()

        s_from = ''  # The lower bound as a string without letters
        s_to = ''  # The upper bound as a string without letters

        last_letter = ''
        cascade = ''
        hold_i = -1
        for i, letter in enumerate(reversed(string), start=0):

            try:
                int(letter)

                if i >= 0:
                    positions[cascade] = 10**hold_i

                s_from = letter + s_from
                s_to = letter + s_to

                hold_i = i
                last_letter = ''
                cascade = ''
                continue
            except ValueError:
                pass

            # else (if it is not an int)

            s_from = '0' + s_from
            s_to = '9' + s_to

            if letter == last_letter:
                cascade = letter + cascade
                continue

            if i >= 0:
                positions[cascade] = 10**hold_i

            cascade = letter
            hold_i = i

            last_letter = letter

        if i >= 0:
            positions[cascade] = 10**hold_i

        del positions['']

        if int(s_to) > 65535:
            s_to = "65535"

        return(positions, s_from, s_to)

    def field_folder(self, folder_path):
        s_field_dir = os.path.basename(folder_path)
        if "-" in s_field_dir:
            self.field_folder_range(folder_path)
        else:
            self.field_folder_positions(folder_path)

    def field_folder_range(self, folder_path):
        s_field_dir = os.path.basename(folder_path)

        # create range

        [s_from, s_to] = s_field_dir.split('-', 1)

        captions = ""
        with open(os.path.join(folder_path, 'captions.txt'), mode='r',
                  closefd=True) as captions_file:
            captions = captions_file.readline()

        from_value = StdValue(s_from, self.datetime)
        to_value = StdValue(s_to, self.datetime)

        range_ = StdCommRange(ases=self.def_as,
                              from_value=from_value,
                              to_value=to_value,
                              observed_only=False,
                              datetime=self.datetime,
                              reference=self.reference,
                              field_captures=captions,
                              source=StdSource.manual,
                              description="")

        self.session.add(range_)
        self.session.commit()

        print(range_)

        i = 0

        for i, filedir in enumerate(os.listdir(folder_path)):
            if (os.path.isfile(os.path.join(folder_path, filedir)) and
                    filedir.endswith('.csv')):
                self.csv_file_field(range_,
                                    os.path.join(folder_path, filedir),
                                    position=1,
                                    shift=0,
                                    group=i)

    def field_folder_positions(self, folder_path):
        s_field_dir = os.path.basename(folder_path)

        # create range

        [s_asn, s_var] = s_field_dir.split(':', 1)

        (asn_positions,
         s_from_asn,
         s_to_asn) = self._find_text_variables(s_asn)
        (var_positions,
         s_from_var,
         s_to_var) = self._find_text_variables(s_var)

        print("FOUND asn variables:", asn_positions,
              "and var variables:", var_positions)

        captions = ""
        with open(os.path.join(folder_path, 'captions.txt'), mode='r',
                  closefd=True) as captions_file:
            captions = captions_file.readline()

        from_value = StdValue(s_from_asn, s_from_var, self.datetime)
        to_value = StdValue(s_to_asn, s_to_var, self.datetime)

        range_ = StdCommRange(ases=self.def_as,
                              from_value=from_value,
                              to_value=to_value,
                              observed_only=False,
                              datetime=self.datetime,
                              reference=self.reference,
                              field_captures=captions,
                              source=StdSource.manual,
                              description="")

        self.session.add(range_)
        self.session.commit()

        print(range_)

        i = 0
        for i, (key, value) in enumerate(var_positions.items(), start=0):
            self.csv_file_field(range_,
                                os.path.join(folder_path, str(key)+'.csv'),
                                value,
                                0,
                                i)

        for j, (key, value) in enumerate(asn_positions.items(), start=i+1):
            self.csv_file_field(range_,
                                os.path.join(folder_path, str(key)+'.csv'),
                                value,
                                16,
                                j)

    def csv_file_field(self, std_comm: StdComm, csv_file: str, position: int,
                       shift: int, group: int):

        with open(csv_file, mode='r', closefd=True) as file:

            reader = csv.DictReader(file, skipinitialspace=True,
                                    fieldnames=('community', 'description'),
                                    delimiter=';')

            i = 0
            for i, row in enumerate(reader):

                if self._skip_row(row):
                    continue

                offset = (int(row['community']) * position) << shift

                field = StdCommField(range_=std_comm, offset=offset,
                                     group=group,
                                     description=row['description'],
                                     datetime=self.datetime)

                self.session.add(field)
                self.session.commit()

                det = RegexDetection(row['description'],
                                     std_comm.from_value + offset, self.session)
                taxos = det.taxonomy(field)

                print('\t', field)
                print(tabulate(field.taxonomies_scopes_to_dict(), headers="keys"))
