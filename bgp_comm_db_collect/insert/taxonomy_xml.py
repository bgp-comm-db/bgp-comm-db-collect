"""
"""

from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.progressbar import Bar
from bgp_comm_db.progressbar import FormatLabel
from bgp_comm_db.progressbar import Percentage
from bgp_comm_db.progressbar import ProgressBar
from bgp_comm_db.tools.country_continent import COUNTRY_CONTINENT
from bgp_comm_db.tools.regex_detection import *
from datetime import datetime
from logging import critical
from logging import debug
from logging import error
from logging import info
from logging import warning
from sqlalchemy.exc import IntegrityError
import itertools
import math
import re
import xml.etree.ElementTree as ET

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['TaxonomyXML']

l_errors_cascade = []
l_errors_single = []


class TaxonomyXML(object):

    """docstring for CsvInsert"""

    DATETIME = datetime(2007, 11, 26, 10, 56, 47)

    def __init__(self, xml_file: str):
        super(TaxonomyXML, self).__init__()
        self.tree = ET.parse(xml_file)
        self.session = None
        self.connection = None
        self.trans = None

    def __enter__(self):
        # connect to the database
        # self.connection = engine.connect()

        # begin a non-ORM transaction
        # self.trans = self.connection.begin()

        # bind an individual Session to the connection
        # self.session = Session(bind=self.connection)
        self.session = Session()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # if exc_type:
        #     self.session.rollback()
        # else:
        widgets = [FormatLabel('Committing ...')]
        pbar = ProgressBar(widgets=widgets).start()

        self.session.commit()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        self.session.close()
        # self.trans.rollback()
        # self.connection.close()  # return connection to the Engine

        pbar.clear_ln('Committing ... Finished')
        # return

    def is_xml_localpref(self, taxonomy) -> bool:
        if isinstance(taxonomy, StdValue):
            return self.is_xml_localpref(taxonomy.taxonomy)

        return (taxonomy.attrib['generaltype'] == "inbound" and
                taxonomy.attrib['subtype'] == "localpref" and
                taxonomy.attrib['subsubtype'] == "unknown")

    def is_xml_prepend(self, taxonomy) -> bool:
        if isinstance(taxonomy, StdValue):
            return self.is_xml_prepend(taxonomy.taxonomy)

        return (taxonomy.attrib['generaltype'] == "outbound" and
                taxonomy.attrib['subtype'] == "routeredistribution" and
                taxonomy.attrib['subsubtype'] == "aspathprepend")

    def is_xml_announcement(self, taxonomy) -> bool:
        if isinstance(taxonomy, StdValue):
            return self.is_xml_announcement(taxonomy.taxonomy)

        return (taxonomy.attrib['generaltype'] == "outbound" and
                taxonomy.attrib['subtype'] == "routeredistribution" and
                taxonomy.attrib['subsubtype'] == "announcement")

    def is_xml_tagging(self, taxonomy) -> bool:
        if isinstance(taxonomy, StdValue):
            return self.is_xml_tagging(taxonomy.taxonomy)

        return (taxonomy.attrib['generaltype'] == "inbound" and
                taxonomy.attrib['subtype'] == "routetagging")

    def is_xml_blackhole(self, taxonomy) -> bool:
        if isinstance(taxonomy, StdValue):
            return self.is_xml_blackhole(taxonomy.taxonomy)

        return (taxonomy.attrib['generaltype'] == "blackhole")

    def is_xml_announcement_skip(self, taxonomy) -> bool:

        if isinstance(taxonomy, StdValue):
            return self.is_xml_announcement_skip(taxonomy.taxonomy)

        if not self.is_xml_announcement(taxonomy):
            raise Exception("is not an announcement.")
        if taxonomy.attrib['characterization'] == "donotannounce":
            return True
        elif taxonomy.attrib['characterization'] == "announce":
            return False
        else:
            raise Exception("Unexpexted characterization value.")

    def get_regex_detection(self, community: StdValue):
        return RegexDetection(community.description, community, self.session)

    def regex_after_to(self, description):
        """returns the string after an 'to' in a sentence"""
        re_to = re.compile(
            "(.*)\s+to\s+(.*)", re.IGNORECASE)

        m_to = re_to.match(description)

        if m_to:
            return m_to.group(2)
        else:
            return None

    @typechecked
    def _gap_decimal_power(self, first: int, second: int) -> optional(int):
        """Checks if the two numbers differ in only one decimal place.

        Args:
            first: The first and in general smaller number.
            second: The second and in general bigger number.

        Returns:
            None: if it differs in more then one decimal place.
            i: the decimal place where it differs so that
                (decimal_place *10^i == second - first) holds.
        """
        gap = math.fabs(second - first)

        for j in itertools.count():
            if 0 < (gap // 10 ** j) < 9 and (gap % 10 ** j) == 0:
                return j
            if gap < 10 ** j:
                return None

    @typechecked
    def _longest_range_prepend(self, lcascade: [StdValue]) -> (
            [StdValue], [StdValue]):
        lchain = []
        lrest = []

        len_lcascade = len(lcascade)

        widgets = [FormatLabel('Finding longest range %(comment)s: '),
                   Percentage(), Bar()]
        pbar = ProgressBar(widgets=widgets, maxval=len_lcascade + 1).start()

        for i, comm_outer in enumerate(lcascade):

            lchain_test = [comm_outer]
            lrest_test = lcascade[:i]

            pbar.update(i + 1, str(comm_outer))

            gap_prev = None
            for j, comm in enumerate(lcascade[i + 1:]):

                prev_comm = lcascade[-1]
                gap = self._gap_decimal_power(comm.value, prev_comm.value)
                if (gap_prev == None) or gap == gap_prev:
                    lchain_test.append(comm)
                else:
                    lrest_test = lrest_test + lcascade[j:]
                    break

            if len(lchain) < len(lchain_test):
                lchain = lchain_test
                lrest = lrest_test

        pbar.clear()

        return (lchain, lrest)

    @typechecked
    def _taxonomy_insert(self, communities, std_comm: StdComm,
                         gap_decimal_power: optional(int)=None):

        if isinstance(communities, list):
            for comm in communities:
                self._taxonomy_insert(
                    comm, std_comm, gap_decimal_power)
            return
        else:
            comm = communities

        detection = self.get_regex_detection(comm)

        l_scopes = []
        l_scopes = detection.scope()

        # if l_scopes and len(l_scopes) > 1:
        #     raise Exception("Multiple scopes detected.")

        if not l_scopes:
            l_scopes = [None]

        for scope in l_scopes:

            if self.is_xml_prepend(comm):
                times = detection.prepend_times()
                if (not isinstance(times, int)
                        and isinstance(gap_decimal_power, int)):

                    times = int(comm.str_value.
                                replace(':', '')[-(gap_decimal_power + 1)])

                prep = StdComm.Prepend(std_comm=std_comm, times=times,
                                       scope=scope, auto_detected=True,
                                       auto_detected_scope=True)
                self.session.add(prep)
                printing = prep

            elif self.is_xml_announcement(comm):
                skip = self.is_xml_announcement_skip(comm)
                anno = StdComm.Announcement(skip=skip,
                                            std_comm=std_comm,
                                            blackhole=False,
                                            scope=scope,
                                            auto_detected=False,
                                            auto_detected_scope=True)
                self.session.add(anno)
                printing = anno

            elif self.is_xml_localpref(comm):
                preference = detection.localpref_preference()
                local = StdComm.LocalPref(preference=preference,
                                          std_comm=std_comm,
                                          scope=scope,
                                          auto_detected=True,
                                          auto_detected_scope=True)
                self.session.add(local)
                printing = local

            elif self.is_xml_tagging(comm):
                tag = StdComm.Tagging(std_comm=std_comm,
                                      scope=scope,
                                      auto_detected=False,
                                      auto_detected_scope=True)
                self.session.add(tag)
                printing = tag

            elif self.is_xml_blackhole(comm):
                black = StdComm.Announcement(skip=True,
                                             std_comm=std_comm,
                                             blackhole=True,
                                             scope=scope,
                                             auto_detected=False,
                                             auto_detected_scope=True)
                self.session.add(black)
                printing = black

        print("\t\t", printing)
        print("\t\t\t", l_scopes)

    @typechecked
    def _single_insert(
            self, communities, asn: int, url: optional(str)) -> void:
        if isinstance(communities, list):
            for community in communities:
                self._single_insert(community, asn, url)
            return
        else:
            comm = communities

        try:
            with self.session.begin_nested():
                std_comm = StdCommRange(
                    ases=Ases.get_at_datetime(self.session, asn,
                                              TaxonomyXML.DATETIME),
                    description=comm.description,
                    reference=url, datetime=TaxonomyXML.DATETIME,
                    from_value=comm, observed_only=False,
                    source=StdSource.xml2007)
                self.session.add(std_comm)
        except IntegrityError as ie:
            print("INTEGRITY-ERROR:", asn)
            print(ie)
            l_errors_single.append((asn, ie))
            return

        print("NORMAL:", std_comm)

        if comm == StdValue(65000, 9385, TaxonomyXML.DATETIME):
            print("REACH 65000:09385, an error might be follwing")

        self._taxonomy_insert(comm, std_comm)

    @typechecked
    def _cascade_insert(self, communities: [StdValue], asn: int,
                        url: optional(str)):
        from_comm = communities[0]
        to_comm = communities[-1]

        # Find min and max
        for comm in communities:
            if comm.value < from_comm.value:
                from_comm = comm
            if comm.value > to_comm.value:
                to_comm = comm

        if from_comm.asn != to_comm.asn:
            info("Beginning and Ending of range has different asn."
                 " begin: %s end %s" % (str(from_comm), str(to_comm)))

        try:
            with self.session.begin_nested():
                std_comm_range = StdCommRange(
                    ases=Ases.get_at_datetime(
                        self.session,
                        asn,
                        TaxonomyXML.DATETIME),
                    from_value=from_comm, to_value=to_comm, description=None,
                    reference=url, datetime=TaxonomyXML.DATETIME,
                    observed_only=False, source=StdSource.xml2007)

                self.session.add(std_comm_range)
        except IntegrityError as ie:
            print("INTEGRITY-ERROR:", asn)
            print(ie)
            l_errors_cascade.append((asn, ie))
            return

        print("CASCADE-RANGE", std_comm_range)

        from_value_prepend = None
        to_value_prepend = to_comm.value

        for comm in communities:
            if self.is_xml_prepend(comm.taxonomy):
                if from_value_prepend == None:
                    from_value_prepend = comm.value
                else:
                    to_value_prepend = comm.value

        for comm in communities:
            assert(comm - from_comm >= 0)
            std_comm_field = StdCommField(datetime=TaxonomyXML.DATETIME,
                                          range_=std_comm_range,
                                          offset=int(comm - from_comm),
                                          group=0,
                                          description=comm.description)
            self.session.add(std_comm_field)
            print("\tCASCASE-FIELD", std_comm_field)

            self._taxonomy_insert(comm, std_comm_field,
                                  self._gap_decimal_power(from_value_prepend,
                                                          to_value_prepend))

    @typechecked
    def _process_lcascade(self, lcascade: list, asn: int, url: optional(str)):
        # FIXME Remove
        # if len(lcascade) > 100:
        #     return

        if len(lcascade) <= 1:
            self._single_insert(communities=lcascade, asn=asn, url=url)
            return

        (lchain, lrest) = self._longest_range_prepend(lcascade)

        self._cascade_insert(communities=lchain, asn=asn, url=url)
        self._single_insert(communities=lrest, asn=asn, url=url)

    def xml_insert(self):
        communitytaxonomy = self.tree.getroot()

        for i, as_ in enumerate(communitytaxonomy):
            asn = int(as_[0].text.strip()[2:])
            name = as_[1].text
            url = as_[2].text
            taxonomylist = as_[3]

            print("#################### AS%s ####################" % (asn))

            lcascade = []

            for j, (value, taxonomy, comment) in enumerate(
                    zip(*[iter(taxonomylist)] * 3)):

                # Split full community value into asn and value
                community = StdValue(value.text, TaxonomyXML.DATETIME)
                community.description = comment.text
                community.taxonomy = taxonomy

                # Scan the community which type it is

                # Following code tests if that is maybe a range
                # lcascade will construct a list with:
                #  - consecutive prepend entries with the same aim
                #  - and a beginning and/or ending element of type announcement
                #      with the same aim
                # "same aim" will be detected if the string in the description
                # after the 'to' is equal
                # Any other type will be ignored or breaks the list
                if self.is_xml_announcement(community):

                    if not lcascade:
                        lcascade.append(community)
                    else:
                        prev_comm = lcascade[-1]

                        prev_detection = self.get_regex_detection(prev_comm)

                        (_, _, after_to_prev) = (prev_detection.
                                                 prepend_times_tuple())

                        after_to = self.regex_after_to(community.description)

                        if (self.is_xml_prepend(prev_comm)
                                and after_to == after_to_prev):
                            lcascade.append(community)
                            self._process_lcascade(lcascade, asn, url)
                            lcascade = []
                        elif self.is_xml_announcement(prev_comm):
                            self._single_insert(lcascade, asn, url)
                            lcascade = [community]
                        else:
                            self._process_lcascade(lcascade, asn, url)
                            lcascade = [community]

                elif self.is_xml_prepend(community):

                    if str(community) == "65004:09839":
                        print("REACH AS END 65004:9839. The following"
                              "calculation needs some time. "
                              "Elements to process:", len(lcascade))

                    if not lcascade:
                        lcascade.append(community)

                    else:
                        prev_comm = lcascade[-1]

                        prev_detection = self.get_regex_detection(prev_comm)

                        detection = self.get_regex_detection(community)

                        after_to_anno = self.regex_after_to(
                            prev_comm.description)

                        (_, _, after_to_prev) = (prev_detection.
                                                 prepend_times_tuple())
                        (_, _, after_to) = detection.prepend_times_tuple()

                        if ((self.is_xml_announcement(prev_comm)
                             and after_to == after_to_anno)
                            or (self.is_xml_prepend(prev_comm)
                                and after_to == after_to_prev)):
                            lcascade.append(community)
                        else:  # happens on a prepends with another comment.
                            self._process_lcascade(lcascade, asn, url)
                            lcascade = [community]

                # at the end of the chain
                elif lcascade:
                    self._process_lcascade(lcascade, asn, url)
                    lcascade = []
                    self.session.commit()

                if (self.is_xml_localpref(community) |
                        self.is_xml_tagging(community) |
                        self.is_xml_blackhole(community)):
                    self._single_insert(community, asn, url)

            # For possibility of announcement/prepend is the last community
            # of the AS
            if lcascade:
                self._process_lcascade(lcascade, asn, url)
                lcascade = []
                self.session.commit()

        with open(proj_dir + '/dublicates.txt', 'w+') as file_dub:

            print("#################### Errors (Cascade) ####################")
            file_dub.write(
                "#################### Errors (Cascade) ####################\n")
            for (asn, err) in l_errors_cascade:
                file_dub.write("\n")
                print("ASN:", asn)
                file_dub.write("ASN: " + str(asn) + "\n")
                print(err)
                file_dub.write(str(err)+"\n")

            print("#################### Errors (Single) ####################")
            file_dub.write(
                "#################### Errors (Single) ####################\n")
            for (asn, err) in l_errors_single:
                file_dub.write("\n")
                print("ASN:", asn)
                file_dub.write("ASN: " + str(asn) + "\n")
                print(err)
                file_dub.write(str(err)+"\n")
